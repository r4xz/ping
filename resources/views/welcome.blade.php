@extends('layouts.default')
@section('title', 'Welcome!')

@section('content')
    <div class="uk-text-center uk-container-center">
        <h1 class="uk-margin-large-top"> Welcome </h1>
        <p> We provide the smallest and the most vulnerable applications. <br>
            @if(Auth::guest())
                <a href="{{ route('user.create') }}"> Join our community! </a>
            @endif
        </p>
    </div>
@endsection