@extends('layouts.default')
@section('title', $user->name)

@section('content')
    <table class="uk-table uk-margin-top">
        <tr>
            <th class="uk-width-1-10"> Id </th>
            <td>{{ $user->id }}</td>
        </tr>
        <tr>
            <th class="uk-width-1-10"> E-Mail </th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th class="uk-width-1-10"> Name </th>
            <td>{{ $user->name }}</td>
        </tr>
    </table>

    @if(Auth::user()->id == $user->id)
        <a class="uk-button" href="{{ action('UserController@edit') }}"> Change password </a>
    @endif
@endsection