@extends('layouts.default')
@section('title', 'Users')

@section('content')
    <table class="uk-table uk-table-striped uk-margin-top">
        <thead>
        <tr>
            <th class="uk-width-1-10"> #</th>
            <th> Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td><a href="{{ route('user.show', $user->id) }}">{{ $user->name }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @include('layouts.elements.pagination', ['paginator' => $users])
@endsection