@extends('layouts.default')
@section('title', 'Create account')

@section('content')
    <div class="uk-width-small-1-2 uk-width-medium-1-4 uk-container-center">
        <form class="uk-form uk-margin-large-top" method="post" action="{{ route('user.store') }}">
            {{ csrf_field() }}
            {{ method_field('post') }}

            @if(count($errors) > 0)
                <div class="uk-alert uk-alert-danger">
                    <strong> There are some errors: </strong>
                    <ul class="uk-margin-top-remove">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="uk-form-row">
                <label class="uk-form-label"> Name: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="text" name="name" value="{{ old('name') }}">
                </div>
            </div>

            <div class="uk-form-row">
                <label class="uk-form-label"> E-Mail: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="text" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="uk-form-row">
                <label class="uk-form-label"> Password: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="password" name="password" value="{{ old('password') }}">
                </div>
            </div>

            <div class="uk-form-row">
                <label class="uk-form-label"> Confirm password: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="password" name="password_confirmation">
                </div>
            </div>

            <div class="uk-form-row">
                <input class="uk-button" type="submit" name="submit" value="Register">
            </div>
        </form>
    </div>
@endsection