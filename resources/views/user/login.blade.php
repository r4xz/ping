@extends('layouts.default')
@section('title', 'Login')

@section('content')
    <div class="uk-width-small-1-2 uk-width-medium-1-4 uk-container-center">
        <form class="uk-form uk-margin-large-top" method="post" action="{{ action('UserController@login') }}">
            {{ csrf_field() }}
            {{ method_field('put') }}

            @if(session()->has('error'))
                <div class="uk-alert uk-alert-danger">{{ session('error') }}</div>
            @endif

            <div class="uk-form-row">
                <label class="uk-form-label"> E-Mail: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="text" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="uk-form-row">
                <label class="uk-form-label"> Password: </label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="password" name="password">
                </div>
            </div>

            <div class="uk-form-row">
                <input class="uk-button" type="submit" name="submit" value="Login">
            </div>
        </form>
    </div>
@endsection