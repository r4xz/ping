<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    @section('styles')
        <link rel="stylesheet" href="{{ asset('css/uikit.almost-flat.min.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/components/notify.almost-flat.min.css') }}"/>
    @show

    @section('scripts')
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/uikit.min.js') }}"></script>
        <script src="{{ asset('js/components/notify.min.js') }}"></script>
    @show
</head>
<body>
@include('layouts.elements.navbar')
<div class="uk-container uk-container-center">
    @yield('content')
</div>

@if(session()->has('notify'))
    <script> UIkit.notify("{{ session('notify') }}", {pos: 'bottom-center'}) </script>
@endif
</body>
</html>