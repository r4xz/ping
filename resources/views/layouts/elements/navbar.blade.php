<nav class="uk-navbar uk-navbar-attached">
    <div class="uk-container uk-container-center">
        <div>
            <ul class="uk-navbar-nav">
                <li><a href="{{ url('/') }}"> Home </a></li>
                <li><a href="{{ route('post.index') }}"> Posts </a></li>
                @if(Auth::check())
                    <li><a href="{{ route('user.index') }}"> Users </a></li>
                @endif
            </ul>
        </div>

        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav">
                @if(Auth::check())
                    <li><a href="{{ route('user.show', Auth::user()->id) }}"> Profile </a></li>
                    <li><a href="{{ action('UserController@logout') }}"> Logout </a></li>
                @else
                    <li><a href="{{ action('UserController@login') }}"> Login </a></li>
                    <li><a href="{{ route('user.create') }}"> Register </a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>