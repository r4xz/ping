@extends('layouts.default')
@section('title', 'Edit post')

@section('content')
    <div class="uk-container-center">
        <form class="uk-form uk-margin-top" method="post" action="{{ route('post.update', $post->id) }}">
            {{ csrf_field() }}
            {{ method_field('put') }}

            @if(count($errors) > 0)
                <div class="uk-alert uk-alert-danger">
                    <strong> There are some errors: </strong>
                    <ul class="uk-margin-top-remove">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="uk-form-row">
                <label class="uk-form-label"> Title: </label>
                <div class="uk-form-controls">
                    <input class="uk-form-width-large" type="text" name="title" value="{{ old('title', $post->title) }}">
                </div>
            </div>

            <div class="uk-form-row">
                <label class="uk-form-label"> Content: </label>
                <div class="uk-form-controls">
                    <textarea class="uk-width-1-1" type="text" name="content">{{ old('content', $post->content) }}</textarea>
                </div>
            </div>

            <div class="uk-form-row">
                <input class="uk-button" type="submit" name="submit" value="Submit">
            </div>
        </form>
    </div>
@endsection