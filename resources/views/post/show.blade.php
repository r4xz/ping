@extends('layouts.default')
@section('title', $post->title)

@section('content')
    @if(Auth::check() && Auth::user()->id == $post->user_id)
        <div class="uk-text-right uk-margin-top">
            <a class="uk-button" href="{{ route('post.edit', $post->id) }}"> Edit </a>
            <form class="uk-display-inline" action="{{ route('post.destroy', $post->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('delete') }}
                <button type="submit" class="uk-button"> Delete </button>
            </form>
        </div>
    @endif

    <div class="uk-margin-large-top uk-margin-large-bottom">
        <article class="uk-article">
            <h1 class="uk-article-title">{{ $post->title }}</h1>
            <p class="uk-article-meta">Written by <a
                        href="{{ route('user.show', $post->user->id) }}">{{ $post->user->name }}</a>
                on {{ $post->created_at }}.</p>
            <p class="uk-article-lead">{!! $post->content !!}</p>
        </article>
    </div>
@endsection