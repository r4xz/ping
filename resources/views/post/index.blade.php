@extends('layouts.default')
@section('title', 'Posts')

@section('content')
    <div class="uk-text-right uk-margin-top">
        <a class="uk-button" href="{{ route('post.create') }}"> Write post </a>
    </div>

    @if(count($posts) == 0)
        <h1> Upss... </h1>
        <p> There aren't any post yet, be first - <a href="{{ route('post.create') }}">write sth</a>! </p>
    @endif

    <div class="uk-margin-top uk-margin-large-bottom">
        @foreach($posts as $post)
            <article class="uk-article">
                <h1 class="uk-article-title">{{ $post->title }}</h1>
                <p class="uk-article-meta">Written by <a
                            href="{{ route('user.show', $post->user->id) }}">{{ $post->user->name }}</a>
                    on {{ $post->created_at }}.</p>
                <p class="uk-article-lead">{{ str_limit(strip_tags($post->content)) }} <a
                            href="{{ route('post.show', $post->id) }}"> read more </a></p>
            </article>
        @endforeach
    </div>

    @include('layouts.elements.pagination', ['paginator' => $posts])
@endsection