<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'except' => ['login', 'attempt', 'create', 'store', 'logout']
        ]);
    }

    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('user.login');
    }

    /**
     * Attempt to login user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function attempt(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            return back()
                ->withInput()
                ->with('error', 'Invalid credentials');
        }

        return redirect()
            ->intended(route('post.index'));
    }

    /**
     * Logout current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect()
            ->action('UserController@login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index', [
            'users' => User::orderBy('name')->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|alpha_dash',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $user = new User($request->only(['name', 'email']));
        $user->password = Hash::make($request->password);
        $user->saveOrFail();

        Auth::login($user);

        return redirect()
            ->route('post.index')
            ->with('notify', 'Account created, you are logged in');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.show', [
            'user' => (object)(DB::select(DB::raw("select * from users where id='$id'"))[0])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        $this->authorize('update', $user);

        return view('user.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $this->authorize('update', $user);

        $this->validate($request, [
            'password' => 'required|confirmed'
        ]);

        $user->password = Hash::make($request->password);
        $user->saveOrFail();

        return redirect()
            ->route('user.show', $user->id)
            ->with('notify', 'Account updated');
    }
}
