<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'UserController@login');
Route::put('login', 'UserController@attempt');
Route::get('logout', 'UserController@logout');
Route::get('user/edit', 'UserController@edit');
Route::put('user/edit', 'UserController@update');

Route::resource('user', 'UserController', ['except' => ['edit', 'update', 'destroy']]);
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');