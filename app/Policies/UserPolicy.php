<?php

namespace App\Policies;

use App\User;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can be updated by authenticated user.
     *
     * @param User $user
     * @param User $userModel
     * @return bool
     */
    public function update(User $user, User $userModel)
    {
        return $user->id == $userModel->id;
    }
}
